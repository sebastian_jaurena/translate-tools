INSERT INTO `culture` (`id`, `code`, `language`, `country`, `name`) VALUES
(1, 'en_US', 'English', 'United State', 'English (United States)'),
(2, 'es_ES', 'Spanish', 'Spain', 'Spanish (Spain)'),
(3, 'pt_BR', 'Portuguese', 'Brazil', 'Portuguese (Brazil)');
