<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body>
    <div class="container">
      <div class="content">
        <div class="navbar">
          <div class="navbar-inner">
            <?php echo link_to(__('Translate Tools'), url_for('@index'), array('class' => 'brand')); ?>
            <ul class="nav">
              <li>
                <?php echo link_to(__('Public projects'), url_for('@projects')); ?>
              </li>
            </ul>
          </div>
        </div>
        <div class="row main">
          <?php echo $sf_content ?>
        </div>
      </div>
      <footer>
        <p>&copy; <?php echo __('Translate Tools'); ?></p>
      </footer>
    </div>
  </body>
</html>
