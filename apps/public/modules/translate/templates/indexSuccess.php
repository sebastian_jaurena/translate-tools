<div class="span8">
  <?php //include_component('view', 'breadcrumb'); ?>
  <h1><?php echo __('Translate Tools'); ?></h1>
  <p><?php echo __('We provide a tool to share documents translations.'); ?></p>
  <p><?php echo __('You have also the posibility to configure the project to not be listed.'); ?></p>
  <p><?php echo __('At the momment we only bring support to XLIFF file.'); ?></p>
  <p>
    <?php echo link_to(__('Help to translate'), url_for('@projects'), array('class' => 'btn')); ?> <?php echo __('or'); ?>
    <?php echo link_to(__('Create your own project'), url_for('@project_new'), array('class' => 'btn')); ?>
  </p>
</div>