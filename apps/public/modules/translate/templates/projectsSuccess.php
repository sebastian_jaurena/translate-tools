<div class="span8">
  <h1><?php echo __('Public projects'); ?></h1>
  <p><?php echo __('List of public projects.'); ?></p>
  <table class="table table-condensed table-hover">
    <thead>
      <th><?php echo __('#'); ?></th>
      <th><?php echo __('Name'); ?></th>
      <th><?php echo __('Owner'); ?></th>
    </thead>
    <tbody>
      <?php foreach($projects as $project): ?>
      <tr>
        <td><?php echo link_to($project->getId(), url_for('@project?hash=' . $project->getHash())); ?></td>
        <td><?php echo link_to($project->getName(), url_for('@project?hash=' . $project->getHash())); ?></td>
        <td><?php echo link_to($project->getOwner(), url_for('@project?hash=' . $project->getHash())); ?></td>
      </tr>
      <?php endforeach; ?>
      <?php if($projects->count() <= 0): ?>
      <tr>
        <td colspan="3"><?php echo __('Projects not found!'); ?></td>
      </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>