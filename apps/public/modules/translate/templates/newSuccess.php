<div class="span8">
  <h1><?php echo __('Create a new project'); ?></h1>
  <form method="post" action="<?php echo url_for('@project_create'); ?>" class="form-horizontal">
    <div class="control-group">
      <label class="control-label" for="name"><?php echo __('Project title'); ?></label>
      <div class="controls">
        <input type="text" id="name" name="name" placeholder="<?php echo __('Name'); ?>" class="input-xlarge">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="description"><?php echo __('Description'); ?></label>
      <div class="controls">
        <textarea id="description" name="description" placeholder="<?php echo __('Description'); ?>" class="input-xlarge"></textarea>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="culture_id"><?php echo __('Select source language'); ?></label>
      <div class="controls">
        <select id="culture_id" name="culture_id">
          <option value=""><?php echo __('Choose culture'); ?></option>
          <?php foreach ($cultures as $culture): ?>
          <option value="<?php echo $culture->getId(); ?>"><?php echo $culture->getName(); ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="owner"><?php echo __('Owner'); ?></label>
      <div class="controls">
        <input type="text" id="owner" name="owner" placeholder="<?php echo __('Your fullname'); ?>" class="input-xlarge">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="mail"><?php echo __('Mail'); ?></label>
      <div class="controls">
        <input type="text" id="mail" name="mail" placeholder="<?php echo __('your@mail.com'); ?>" class="input-xlarge">
      </div>
    </div>
    <div class="control-group">
      <div class="controls">
        <label class="checkbox">
          <input type="checkbox" name="public" value="1"> <?php echo __('Is public?'); ?>
        </label>
      </div>
    </div>
    <div class="form-actions">
      <button type="submit" class="btn btn-primary"><?php echo __('Create project'); ?></button>
      <?php echo link_to(__('Cancel'), url_for('@homepage'), array('class' => 'btn')); ?>
    </div>
  </form>
</div>