<div class="span8">
  <h1><?php echo __('Project details'); ?></h1>
  <dl class="dl-horizontal">
    <dt><?php echo __('Share link'); ?></dt>
    <dd><input type="text" value="<?php echo url_for('@project?hash=' . $project->getHash(), true); ?>" class="input-xlarge" /></dd>
    <dt><?php echo __('ID'); ?></dt>
    <dd><?php echo $project->getHash(); ?></dd>
    <dt><?php echo __('Title'); ?></dt>
    <dd><?php echo $project->getName() ? $project->getName() : '-'; ?></dd>
    <dt><?php echo __('Description'); ?></dt>
    <dd><?php echo $project->getDescription(); ?></dd>
    <dt><?php echo __('Original language'); ?></dt>
    <dd><?php echo $project->getCulture()->getName(); ?></dd>
    <dt><?php echo __('Project owner'); ?></dt>
    <dd><?php echo $project->getOwner(); ?></dd>
    <dt><?php echo __('Mail'); ?></dt>
    <dd><?php echo $project->getMail(); ?></dd>
  </dl>
  <form method="post" action="<?php echo url_for('@project_add_file'); ?>" class="form-inline" enctype="multipart/form-data">
    <legend>Add file</legend>
    <input type="hidden" name="project_id" value="<?php echo $project->getId(); ?>" />
    <?php echo __('Select XLIFF file'); ?> <input type="file" name="xliff" id="xliff" />
    <button type="submit" class="btn"><?php echo __('Upload'); ?></button>
  </form>
  <h2><?php echo __('Files'); ?></h2>
  <table class="table">
    <thead>
      <tr>
        <th><?php __('File'); ?></th>
        <th><?php __('Language'); ?></th>
        <th><?php __('Status'); ?></th>
        <th><?php __('Progress'); ?></th>
        <th><?php __('Action'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($files as $file): ?>
      <tr class="<?php echo $file->getTranslationStatus()->getStatusColor(); ?>">
        <td><?php echo link_to($file->getName(), url_for('@project_file?hash='.$project->getHash().'&id='.$file->getId())) ?></td>
        <td><?php echo link_to($file->getCulture()->getName(), url_for('@project_file?hash='.$project->getHash().'&id='.$file->getId())) ?></td>
        <td><?php echo link_to($file->getTranslationStatus()->getName(), url_for('@project_file?hash='.$project->getHash().'&id='.$file->getId())) ?></td>
        <td><?php echo link_to($file->getProgress() . '%', url_for('@project_file?hash='.$project->getHash().'&id='.$file->getId())); ?></td>
        <td>
          <a href="<?php echo url_for('@project_download_file?hash=' . $project->getHash() . '&id=' . $file->getId()); ?>"><span class="icon-download-alt"></span></a>
          <a href="<?php echo url_for('@project_delete_file?hash=' . $project->getHash() . '&id=' . $file->getId()); ?>"><span class="icon-trash"></span></a>
        </td>
      </tr>
      <?php endforeach; ?>
      <?php if($files->count() <= 0): ?>
      <tr>
        <td colspan="4">
          <?php echo __('Please, upload a file to be translated!'); ?>
        </td>
      </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>