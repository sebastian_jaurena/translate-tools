<div class="span8">
  <?php include_component('view', 'breadcrumb', array('crumbs' => array('Translate Xliff file'))); ?>
  <form method="post" action="/" enctype="multipart/form-data" class="form-inline">
    <div class="form-actions">
      <input id="xliff" type="file" name="xliff">
      <button type="submit" class="btn btn-primary">Upload</button>
    </div>
  </form>
  <?php if(isset($uploaded) && $uploaded): ?>
  <form method="post" action="<?php echo url_for('save'); ?>" target="translate">
    <input type="hidden" name="name" value="<?php echo $name; ?>">
    <input type="hidden" name="version" value="<?php echo $version; ?>">
    <input type="hidden" name="original" value="<?php echo $original; ?>">
    <input type="hidden" name="source-language" value="<?php echo $source_language; ?>">
    <input type="hidden" name="datatype" value="<?php echo $datatype; ?>">
    <legend>Translate (<?php echo $source_language; ?>)</legend>
    <?php foreach($units as $unit): ?>
    <div class="row">
      <div class="span4">
        <input type="hidden" name="source[<?php echo (string)$unit->attributes()->id ?>]" value="<?php echo (string)$unit->source ?>">
        <?php echo (string)$unit->source ?>
      </div>
      <div class="span4">
        <textarea class="span4" name="target[<?php echo (string)$unit->attributes()->id ?>]"><?php echo (string)$unit->target ?></textarea>
      </div>
    </div>
    <?php endforeach; ?>
    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Save</button>
    </div>
  </form>
  <?php endif; ?>
</div>