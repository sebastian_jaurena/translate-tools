<div class="span8">
  <h1>
    <?php echo __('File details'); ?>
    <?php echo link_to(__('Download'), url_for('@project_download_file?hash=' . $file->getProject()->getHash() . '&id=' . $file->getId()), array('class' => 'btn')) ?>
  </h1>
  <dl class="dl-horizontal">
    <dt><?php echo __('File'); ?></dt>
    <dd><?php echo $file->getName(); ?></dd>
    <dt><?php echo __('Source language'); ?></dt>
    <dd><?php echo $file->getProject()->getCulture()->getName(); ?></dd>
    <dt><?php echo __('Target language'); ?></dt>
    <dd><?php echo $file->getCulture()->getName(); ?></dd>
    <dt><?php echo __('Translation status'); ?></dt>
    <dd><?php echo $file->getTranslationStatus()->getName(); ?></dd>
    <dt><?php echo __('Translation progress'); ?></dt>
    <dd><?php echo $file->getProgress(); ?>%</dd>
  </dl>
  <form method="post" action="<?php echo url_for('@project_translate_file'); ?>">
    <input type="hidden" name="file_id" value="<?php echo $file->getId(); ?>" />
    <table class="table table-condensed table-striped">
      <thead>
        <tr>
          <th><?php echo __('Text to be translated'); ?></th>
          <th><?php echo __('Translation'); ?></th>
          <th><?php echo __('Status'); ?></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($translations as $translation): ?>
        <tr class="<?php echo $translation->getTranslationStatus()->getStatusColor(); ?>">
          <td>
            <input type="hidden" name="translation[<?php echo $translation->getId() ?>][id]" value="<?php echo $translation->getId() ?>" />
            <?php echo $translation->getText(); ?>
          </td>
          <td>
            <textarea class="input-large" name="translation[<?php echo $translation->getId() ?>][translated]"><?php echo $translation->getTranslated(); ?></textarea>
          </td>
          <td>
            <select name="translation[<?php echo $translation->getId() ?>][status_id]" class="input-medium">
              <?php foreach($status as $stat): ?>
              <option value="<?php echo $stat->getId() ?>"<?php if($stat->getId() == $translation->getStatusId()): ?>selected="selected"<?php endif; ?>>
                <?php echo $stat->getName(); ?>
              </option>
              <?php endforeach; ?>
            </select>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <div class="form-actions">
      <button type="submit" class="btn btn-primary"><?php echo __('Translate'); ?></button>
      <?php echo link_to(__('Back'), url_for('@project?hash=' . $file->getProject()->getHash()), array('class' => 'btn')) ?>
    </div>
  </form>
</div>