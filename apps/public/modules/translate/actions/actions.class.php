<?php

class translateActions extends sfActions
{
  /**
   * Executes index action
   *
   * @param sfRequest $request A request object
   */
  public function executeIndex(sfWebRequest $request)
  {
  }

  public function executeProjects(sfWebRequest $request)
  {
    $this->projects = Doctrine::getTable('Project')->findByPublic(1);
  }

  public function executeProject(sfWebRequest $request)
  {
    $this->project = Doctrine::getTable('Project')->getProjectByHash($request->getParameter('hash'));
    if($this->project)
    {
      $this->files = Doctrine::getTable('File')->findByProjectId($this->project->getId());
    }
    else
    {
      $this->redirect404();
    }
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->cultures = Doctrine::getTable('Culture')->findAll();
  }

  public function executeCreate(sfWebRequest $request)
  {
    do{
      $hash = md5(time());
      $projects = Doctrine::getTable('Project')->findByHash($hash);
    }while($projects->count() > 0);

    $project = new Project();
    $project->setHash($hash);
    $project->setName($request->getParameter('name'));
    $project->setDescription($request->getParameter('description'));
    $project->setCultureId($request->getParameter('culture_id'));
    $project->setOwner($request->getParameter('owner'));
    $project->setMail($request->getParameter('mail'));
    $project->setPublic($request->getParameter('public'));
    $project->save();

    $this->redirect('@project?hash=' . $hash);
  }

  public function executeImport(sfWebRequest $request)
  {
    $this->uploaded = false;

    $project_id = $request->getParameter('project_id');

    $project = Doctrine::getTable('Project')->findOneById($project_id);

    if(!$project)
    {
      $this->redirect404();
    }
    
    $xml = $request->getFiles('xliff');

    if(!empty($xml))
    {
      $xliff = simplexml_load_file($xml['tmp_name']);

      $name     = $xml['name'];
      $version  = (string)$xliff->attributes()->{'version'};
      $original = (string)$xliff->file->attributes()->{'original'};
      $language = (string)$xliff->file->attributes()->{'source-language'};
      $datatype = (string)$xliff->file->attributes()->{'datatype'};
      $units    = $xliff->file->body->{'trans-unit'};

      $culture = Doctrine::getTable('Culture')->findOneByCode($language);

      if($culture)
      {
        $file = new File();
        $file->setProjectId($project_id);
        $file->setCultureId($culture->getId());
        $file->setName($name);
        $file->setVersion($version);
        $file->setOriginal($original);
        $file->setDatatype($datatype);
        $file->setStatusId(Status::Open);
        $file->save();

        $file_id = $file->getId();

        foreach ($units as $unit) {
          $translation = new Translation();
          $translation->setFileId($file_id);
          $translation->setTranslationId((string)$unit->attributes()->id);
          $translation->setText((string)$unit->source);
          $translation->setTranslated((string)$unit->target);
          $translation->setStatusId(Status::Open);
          $translation->save();
        }
      }
    }

    $this->redirect('@project?hash='.$project->getHash());
  }

  public function executeFile(sfWebRequest $request)
  {
    $this->file = Doctrine::getTable('File')->findOneById($request->getParameter('id'));
    
    if(!$this->file)
    {
      $this->redirect('@project?hash=' . $request->getParameter('hash'));
    }

    $this->translations = Doctrine::getTable('Translation')->findByFileId($this->file->getId());
    $this->status = Doctrine::getTable('TranslationStatus')->findAll();
  }

  public function executeTranslate(sfWebRequest $request)
  {
    $file_id = $request->getParameter('file_id');
    $translations = $request->getParameter('translation');
    
    $file = Doctrine::getTable('File')->findOneById($file_id);

    if(!$file)
    {
      $this->redirect404();
    }

    $isCompleted = true;
    $file_status_id = Status::Open;
    $progress = 0;

    $progress = 0;
    $total = 0;
    $count = 0;

    foreach($translations as $translation)
    {
      $id = $translation['id'];
      $status_id = $translation['status_id'];
      $translated = $translation['translated'];
      $temp = Doctrine::getTable('Translation')->findOneById($id);
      
      if($temp)
      {
        $temp->setStatusId($status_id);
        $temp->setTranslated($translated);
        $temp->save();

        if($status_id > Status::Open)
        {
          $file_status_id = $status_id;
        }

        if($status_id < Status::Completed)
        {
          $isCompleted = false;
        }

        $count += $status_id - 1;
        $total++;
      }

      if($file_status_id > Status::Open)
      {
        $file_status_id = $isCompleted ? Status::Completed : Status::InProgress;
      }

      if($total > 0)
      {
        $progress = round($count * 100 / ($total * 2));
      }

      $file->setStatusId($file_status_id);
      $file->setProgress($progress);
      $file->save();
    }
    $this->redirect('@project_file?hash='.$file->getProject()->getHash() . '&id=' . $file->getId());
  }

  public function executeDownload(sfWebRequest $request)
  {
    $hash = $request->getParameter('hash');
    $id = $request->getParameter('id');
    
    $project = Doctrine::getTable('Project')->findOneByHash($hash);
    $file = Doctrine::getTable('File')->findOneById($id);

    if(!$project || !$file || ($project->getId() != $file->getProjectId()))
    {
      $this->redirect404();
    }

    $filename = $file->getName();
    $version = $file->getVersion();
    $original = $file->getOriginal();
    $source = $project->getCulture()->getCode();
    $target = $file->getCulture()->getCode();
    $datatype = $file->getDatatype();

    $translations = Doctrine::getTable('Translation')->findByFileId($id);

    $this->getResponse()->setHttpHeader('Content-type', 'text/xml');
    $this->getResponse()->setHttpHeader('content-disposition', 'attachment; filename=' . $filename);

    $xliff = new SimpleXMLElement('<xliff></xliff>');
    $xliff->addAttribute('version', $version);

    $file = $xliff->addChild('file');
    $file->addAttribute('original', $original);
    $file->addAttribute('source-language', $source);
    $file->addAttribute('target-language', $target);
    $file->addAttribute('datatype', $datatype);

    $body = $file->addChild('body');

    foreach($translations as $translation){
      $unit = $body->addChild('trans-unit');
      $unit->addAttribute('id', $translation->getTranslationId());
      $unit->addChild('source', $translation->getText());
      $unit->addChild('target', $translation->getTranslated());
    }

    sfView::NONE;

    return $this->renderText($xliff->saveXML());
  }

  public function executeDelete(sfWebRequest $request) {
    $hash = $request->getParameter('hash');
    $file_id = $request->getParameter('id');
    
    $project = Doctrine::getTable('Project')->findOneByHash($hash);
    $file = Doctrine::getTable('File')->findOneById($file_id);

    if(!$project || !$file || ($project->getId() != $file->getProjectId()))
    {
      $this->redirect404();
    }

    $file->delete();
    $this->redirect('@project?hash='.$file->getProject()->getHash());
  }
}