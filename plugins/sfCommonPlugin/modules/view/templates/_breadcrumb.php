<ul class="breadcrumb">
  <li class="active"><?php echo __('Home'); ?></li>
  <?php if(!empty($crumbs)): ?>
  <?php foreach($crumbs as $page): ?>
  <li class="active"><span class="divider">/</span><?php echo $page; ?></li>
  <?php endforeach; ?>
  <?php endif; ?>
</ul>