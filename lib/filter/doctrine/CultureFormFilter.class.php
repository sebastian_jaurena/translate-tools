<?php

/**
 * Culture filter form.
 *
 * @package    translate
 * @subpackage filter
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CultureFormFilter extends BaseCultureFormFilter
{
  public function configure()
  {
  }
}
