<?php

/**
 * Translation filter form.
 *
 * @package    translate
 * @subpackage filter
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TranslationFormFilter extends BaseTranslationFormFilter
{
  public function configure()
  {
  }
}
