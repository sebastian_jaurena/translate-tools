<?php

/**
 * Translation filter form base class.
 *
 * @package    translate
 * @subpackage filter
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'file_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('File'), 'add_empty' => true)),
      'translation_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'text'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'translated'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'status_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TranslationStatus'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'file_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('File'), 'column' => 'id')),
      'translation_id' => new sfValidatorPass(array('required' => false)),
      'text'           => new sfValidatorPass(array('required' => false)),
      'translated'     => new sfValidatorPass(array('required' => false)),
      'status_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TranslationStatus'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Translation';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'file_id'        => 'ForeignKey',
      'translation_id' => 'Text',
      'text'           => 'Text',
      'translated'     => 'Text',
      'status_id'      => 'ForeignKey',
    );
  }
}
