<?php

/**
 * Project filter form base class.
 *
 * @package    translate
 * @subpackage filter
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseProjectFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'        => new sfWidgetFormFilterInput(),
      'description' => new sfWidgetFormFilterInput(),
      'hash'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'culture_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Culture'), 'add_empty' => true)),
      'owner'       => new sfWidgetFormFilterInput(),
      'mail'        => new sfWidgetFormFilterInput(),
      'public'      => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'name'        => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorPass(array('required' => false)),
      'hash'        => new sfValidatorPass(array('required' => false)),
      'culture_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Culture'), 'column' => 'id')),
      'owner'       => new sfValidatorPass(array('required' => false)),
      'mail'        => new sfValidatorPass(array('required' => false)),
      'public'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('project_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Project';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'name'        => 'Text',
      'description' => 'Text',
      'hash'        => 'Text',
      'culture_id'  => 'ForeignKey',
      'owner'       => 'Text',
      'mail'        => 'Text',
      'public'      => 'Boolean',
    );
  }
}
