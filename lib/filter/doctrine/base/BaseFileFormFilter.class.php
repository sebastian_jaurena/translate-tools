<?php

/**
 * File filter form base class.
 *
 * @package    translate
 * @subpackage filter
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseFileFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'project_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Project'), 'add_empty' => true)),
      'culture_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Culture'), 'add_empty' => true)),
      'status_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TranslationStatus'), 'add_empty' => true)),
      'progress'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'name'       => new sfWidgetFormFilterInput(),
      'version'    => new sfWidgetFormFilterInput(),
      'original'   => new sfWidgetFormFilterInput(),
      'datatype'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'project_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Project'), 'column' => 'id')),
      'culture_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Culture'), 'column' => 'id')),
      'status_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TranslationStatus'), 'column' => 'id')),
      'progress'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'name'       => new sfValidatorPass(array('required' => false)),
      'version'    => new sfValidatorPass(array('required' => false)),
      'original'   => new sfValidatorPass(array('required' => false)),
      'datatype'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('file_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'File';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'project_id' => 'ForeignKey',
      'culture_id' => 'ForeignKey',
      'status_id'  => 'ForeignKey',
      'progress'   => 'Number',
      'name'       => 'Text',
      'version'    => 'Text',
      'original'   => 'Text',
      'datatype'   => 'Text',
    );
  }
}
