<?php

/**
 * Culture filter form base class.
 *
 * @package    translate
 * @subpackage filter
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCultureFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'     => new sfWidgetFormFilterInput(),
      'language' => new sfWidgetFormFilterInput(),
      'country'  => new sfWidgetFormFilterInput(),
      'name'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'code'     => new sfValidatorPass(array('required' => false)),
      'language' => new sfValidatorPass(array('required' => false)),
      'country'  => new sfValidatorPass(array('required' => false)),
      'name'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('culture_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Culture';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'code'     => 'Text',
      'language' => 'Text',
      'country'  => 'Text',
      'name'     => 'Text',
    );
  }
}
