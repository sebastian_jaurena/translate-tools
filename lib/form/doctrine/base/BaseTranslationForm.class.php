<?php

/**
 * Translation form base class.
 *
 * @method Translation getObject() Returns the current form's model object
 *
 * @package    translate
 * @subpackage form
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTranslationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'file_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('File'), 'add_empty' => false)),
      'translation_id' => new sfWidgetFormInputText(),
      'text'           => new sfWidgetFormTextarea(),
      'translated'     => new sfWidgetFormTextarea(),
      'status_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TranslationStatus'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'file_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('File'))),
      'translation_id' => new sfValidatorString(array('max_length' => 10)),
      'text'           => new sfValidatorString(array('max_length' => 4000)),
      'translated'     => new sfValidatorString(array('max_length' => 4000)),
      'status_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TranslationStatus'))),
    ));

    $this->widgetSchema->setNameFormat('translation[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Translation';
  }

}
