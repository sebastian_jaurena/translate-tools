<?php

/**
 * File form base class.
 *
 * @method File getObject() Returns the current form's model object
 *
 * @package    translate
 * @subpackage form
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'project_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Project'), 'add_empty' => false)),
      'culture_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Culture'), 'add_empty' => false)),
      'status_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TranslationStatus'), 'add_empty' => false)),
      'progress'   => new sfWidgetFormInputText(),
      'name'       => new sfWidgetFormInputText(),
      'version'    => new sfWidgetFormInputText(),
      'original'   => new sfWidgetFormInputText(),
      'datatype'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'project_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Project'))),
      'culture_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Culture'))),
      'status_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TranslationStatus'))),
      'progress'   => new sfValidatorInteger(),
      'name'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'version'    => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'original'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'datatype'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('file[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'File';
  }

}
