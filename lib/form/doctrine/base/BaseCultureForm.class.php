<?php

/**
 * Culture form base class.
 *
 * @method Culture getObject() Returns the current form's model object
 *
 * @package    translate
 * @subpackage form
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCultureForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'       => new sfWidgetFormInputHidden(),
      'code'     => new sfWidgetFormInputText(),
      'language' => new sfWidgetFormInputText(),
      'country'  => new sfWidgetFormInputText(),
      'name'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'     => new sfValidatorString(array('max_length' => 8, 'required' => false)),
      'language' => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'country'  => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'name'     => new sfValidatorString(array('max_length' => 40, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Culture', 'column' => array('code')))
    );

    $this->widgetSchema->setNameFormat('culture[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Culture';
  }

}
