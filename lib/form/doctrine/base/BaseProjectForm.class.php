<?php

/**
 * Project form base class.
 *
 * @method Project getObject() Returns the current form's model object
 *
 * @package    translate
 * @subpackage form
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseProjectForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'name'        => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormTextarea(),
      'hash'        => new sfWidgetFormInputText(),
      'culture_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Culture'), 'add_empty' => true)),
      'owner'       => new sfWidgetFormInputText(),
      'mail'        => new sfWidgetFormInputText(),
      'public'      => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description' => new sfValidatorString(array('max_length' => 4000, 'required' => false)),
      'hash'        => new sfValidatorString(array('max_length' => 32)),
      'culture_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Culture'), 'required' => false)),
      'owner'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'mail'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'public'      => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Project', 'column' => array('hash')))
    );

    $this->widgetSchema->setNameFormat('project[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Project';
  }

}
