<?php

/**
 * BaseFile
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $project_id
 * @property integer $culture_id
 * @property integer $status_id
 * @property integer $progress
 * @property string $name
 * @property string $version
 * @property string $original
 * @property string $datatype
 * @property Project $Project
 * @property Culture $Culture
 * @property TranslationStatus $TranslationStatus
 * @property Doctrine_Collection $Translation
 * 
 * @method integer             getProjectId()         Returns the current record's "project_id" value
 * @method integer             getCultureId()         Returns the current record's "culture_id" value
 * @method integer             getStatusId()          Returns the current record's "status_id" value
 * @method integer             getProgress()          Returns the current record's "progress" value
 * @method string              getName()              Returns the current record's "name" value
 * @method string              getVersion()           Returns the current record's "version" value
 * @method string              getOriginal()          Returns the current record's "original" value
 * @method string              getDatatype()          Returns the current record's "datatype" value
 * @method Project             getProject()           Returns the current record's "Project" value
 * @method Culture             getCulture()           Returns the current record's "Culture" value
 * @method TranslationStatus   getTranslationStatus() Returns the current record's "TranslationStatus" value
 * @method Doctrine_Collection getTranslation()       Returns the current record's "Translation" collection
 * @method File                setProjectId()         Sets the current record's "project_id" value
 * @method File                setCultureId()         Sets the current record's "culture_id" value
 * @method File                setStatusId()          Sets the current record's "status_id" value
 * @method File                setProgress()          Sets the current record's "progress" value
 * @method File                setName()              Sets the current record's "name" value
 * @method File                setVersion()           Sets the current record's "version" value
 * @method File                setOriginal()          Sets the current record's "original" value
 * @method File                setDatatype()          Sets the current record's "datatype" value
 * @method File                setProject()           Sets the current record's "Project" value
 * @method File                setCulture()           Sets the current record's "Culture" value
 * @method File                setTranslationStatus() Sets the current record's "TranslationStatus" value
 * @method File                setTranslation()       Sets the current record's "Translation" collection
 * 
 * @package    translate
 * @subpackage model
 * @author     Sebastian Jaurena
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseFile extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('file');
        $this->hasColumn('project_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('culture_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('status_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('progress', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 4,
             ));
        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('version', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('original', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('datatype', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Project', array(
             'local' => 'project_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('Culture', array(
             'local' => 'culture_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('TranslationStatus', array(
             'local' => 'status_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasMany('Translation', array(
             'local' => 'id',
             'foreign' => 'file_id'));
    }
}